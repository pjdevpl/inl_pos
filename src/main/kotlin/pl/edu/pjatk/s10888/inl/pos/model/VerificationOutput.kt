package pl.edu.pjatk.s10888.inl.pos.model

data class VerificationOutput(val precision: Double)