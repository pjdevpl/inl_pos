package pl.edu.pjatk.s10888.inl.pos.components.pipeline.variants

import org.cleartk.ml.feature.extractor.CleartkExtractor
import org.cleartk.ml.feature.function.FeatureFunctionExtractor
import org.cleartk.token.type.Token
import java.lang.RuntimeException

interface FeatureSet {

    val tokenFeatureExtractor: FeatureFunctionExtractor<Token>
    val contextFeatureExtractor: CleartkExtractor<Token, Token>

    companion object {
        fun getFeatureSet(id: String): FeatureSet {
            return when (id) {
                "A" -> FeatureSetA()
                "B" -> FeatureSetB()
                "C" -> FeatureSetC()
                else -> throw RuntimeException("Unknown featureset")
            }
        }
    }
}