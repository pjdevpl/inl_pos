package pl.edu.pjatk.s10888.inl.pos.model

data class POSTag(val tag: String) {

    fun getFirstLevelTag(): String {
        return tag.split(":").first()
    }
}