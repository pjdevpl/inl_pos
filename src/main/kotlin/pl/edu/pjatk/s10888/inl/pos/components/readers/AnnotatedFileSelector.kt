package pl.edu.pjatk.s10888.inl.pos.components.readers

import pl.edu.pjatk.s10888.inl.pos.model.AnnotatedSentence

class AnnotatedFileSelector(private val sentences: List<AnnotatedSentence>) {

    fun selectPercentage(percentageStart: Double, percentageEnd: Double): List<AnnotatedSentence> {
        val startIndex = sentences.size * (percentageStart / 100)
        val endIndex = sentences.size * (percentageEnd / 100)

        val selectedSentences = sentences.subList(startIndex.toInt(), endIndex.toInt())
        val startingPoint = selectedSentences.first().indexStart
        return selectedSentences.map { sentence ->
            sentence.copy(annotatedWords = sentence.annotatedWords.map { word -> word.copy(
                    indexStart = word.indexStart - startingPoint,
                    indexEnd = word.indexEnd - startingPoint
            ) }, indexStart = sentence.indexStart - startingPoint, indexEnd = sentence.indexEnd - startingPoint)
        }
    }

}