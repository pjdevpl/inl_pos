package pl.edu.pjatk.s10888.inl.pos.components.pipeline

import org.apache.uima.UimaContext
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase
import org.apache.uima.fit.descriptor.ConfigurationParameter
import org.apache.uima.jcas.JCas
import org.apache.uima.util.Level
import org.cleartk.token.type.Sentence
import org.cleartk.token.type.Token
import org.cleartk.util.ViewUriUtil
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileParser
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileReader
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileSelector
import pl.edu.pjatk.s10888.inl.pos.model.AnnotatedDocument
import pl.edu.pjatk.s10888.inl.pos.utils.ParamsInitializer
import java.io.File

class CorpusReader: JCasAnnotator_ImplBase() {

    companion object {
        const val PARAM_PERCENTAGE_START = "paramPercentageStart"
        const val PARAM_PERCENTAGE_END = "paramPercentageEnd"
    }

    @ConfigurationParameter(name = PARAM_PERCENTAGE_START, mandatory = true, description = "defines, lowest percentage boundary, for sentences selection")
    var percentageStart: Float? = null

    @ConfigurationParameter(name = PARAM_PERCENTAGE_END, mandatory = true, description = "defines, lowest percentage boundary, for sentences selection")
    var percentageEnd: Float? = null

    override fun initialize(aContext: UimaContext?) {
        super.initialize(aContext)
        ParamsInitializer.initializeParameters(this, context)
    }

    override fun process(aJCas: JCas) {
        val uri = ViewUriUtil.getURI(aJCas)
        val file = File(uri)

        val reader = AnnotatedFileReader(file)
        val lines = reader.readLinesFromCorpus()

        val parser = AnnotatedFileParser(lines)
        val sentences = parser.recognizeSentences()

        val selector = AnnotatedFileSelector(sentences)
        val selectedSentences = selector.selectPercentage(percentageStart!!.toDouble(), percentageEnd!!.toDouble())
        val document = AnnotatedDocument(selectedSentences)
        val plainDocument = document.makeDocument()

        aJCas.setSofaDataString(plainDocument, "text/plain")

        document.annotatedSentences.forEach { s ->
            val uimaSentence = Sentence(aJCas, s.indexStart, s.indexEnd)
            uimaSentence.addToIndexes()
            s.annotatedWords.forEach { word ->
                val uimaToken = Token(aJCas, word.indexStart, word.indexEnd)
                uimaToken.pos = word.tag.getFirstLevelTag()
                uimaToken.addToIndexes()
            }
        }
    }
}