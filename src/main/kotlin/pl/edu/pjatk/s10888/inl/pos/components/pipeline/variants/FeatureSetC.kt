package pl.edu.pjatk.s10888.inl.pos.components.pipeline.variants

import org.cleartk.ml.feature.extractor.CleartkExtractor
import org.cleartk.ml.feature.extractor.CoveredTextExtractor
import org.cleartk.ml.feature.function.*
import org.cleartk.token.type.Token

class FeatureSetC: FeatureSet {

    override val tokenFeatureExtractor = FeatureFunctionExtractor(
            CoveredTextExtractor<Token>(),
            LowerCaseFeatureFunction(),
            CapitalTypeFeatureFunction(),
            NumericTypeFeatureFunction())

    override val contextFeatureExtractor = CleartkExtractor<Token, Token>(
            Token::class.java,
            CoveredTextExtractor(),
            CleartkExtractor.Preceding(2),
            CleartkExtractor.Following(2))
}