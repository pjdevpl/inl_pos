package pl.edu.pjatk.s10888.inl.pos.model

data class AnnotatedSentence(val annotatedWords: Collection<AnnotatedCorpusEntry>, val indexStart: Int = 0, val indexEnd: Int = 0) {

    fun withDocumentPosition(indexStart: Int, indexEnd: Int): AnnotatedSentence {
        return this.copy(indexStart = indexStart, indexEnd = indexEnd)
    }

    fun makeSentence(): String {
        val stringBuilder = StringBuilder()
        annotatedWords.forEach { word ->
            stringBuilder.append(word.word)
            stringBuilder.append(" ")
        }
        return stringBuilder.toString()
    }

}