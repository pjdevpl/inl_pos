package pl.edu.pjatk.s10888.inl.pos

import org.apache.uima.UIMAFramework
import org.cleartk.ml.jar.Train
import org.apache.uima.fit.pipeline.SimplePipeline
import org.apache.uima.fit.component.ViewCreatorAnnotator
import org.apache.uima.fit.factory.AnalysisEngineFactory
import org.apache.uima.fit.factory.AggregateBuilder
import org.cleartk.util.cr.UriCollectionReader
import org.apache.uima.util.Level
import org.cleartk.ml.crfsuite.CrfSuiteWrapper
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.CorpusReader
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.POSAnnotator
import java.io.File
import org.cleartk.ml.jar.JarClassifierBuilder
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.AnnotatedOutputWriter
import pl.edu.pjatk.s10888.inl.pos.components.verification.AnnotationVerificator
import pl.edu.pjatk.s10888.inl.pos.model.VerificationOutput


fun main(args: Array<String>) {
    val trainer = TrainerMain()
    trainer.run()
}

class TrainerMain {

    private val corpusPath = File(this.javaClass.getResource("/nkjp_tab.txt").toURI())
    private fun createCorpusReader() = UriCollectionReader.getCollectionReaderFromFiles(listOf(corpusPath))!!

    private val modelOutput = "./output"
    private val classificationOutput = "./output/classification"
    private val classificationOutputFile = "classification_output.txt"

    fun run() {
        val selectedModel = listOf("A", "B", "C").map { featureSet ->
            train(featureSet, 0.0, 80.0)
            validate(featureSet, 80.0, 90.0)
        }.maxBy { it.second.precision }

        println("Selected model: ${selectedModel!!.first}")
        println("Testing selected model...")
        val result = validate(selectedModel.first, 90.0, 100.0)
        println("Result of the best model: ${result.second.precision}")
    }

    private fun train(featureSet: String, percentageStart: Double, percentageEnd: Double) {
        val builder = AggregateBuilder()
        val corpusReader = createCorpusReader()
        corpusReader.uimaContext.logger.setLevel(Level.OFF)
        UIMAFramework.getLogger(CrfSuiteWrapper::class.java).setLevel(Level.OFF)

        println("Preparing model $featureSet")
        builder.add(AnalysisEngineFactory.createEngineDescription(
                ViewCreatorAnnotator::class.java,
                ViewCreatorAnnotator.PARAM_VIEW_NAME,
                "default"))
        builder.add(AnalysisEngineFactory.createEngineDescription(
                CorpusReader::class.java,
                CorpusReader.PARAM_PERCENTAGE_START, percentageStart.toFloat(),
                CorpusReader.PARAM_PERCENTAGE_END, percentageEnd.toFloat()
        ))
        builder.add(POSAnnotator.getWriterDescription(File(modelOutput, featureSet).absolutePath, featureSet))
        SimplePipeline.runPipeline(corpusReader, builder.createAggregateDescription())

        println("Training model $featureSet")
        Train.main(File(modelOutput, featureSet).absolutePath)
    }

    private fun validate(featureSet: String, percentageStart: Double, percentageEnd: Double): Pair<String, VerificationOutput> {
        val builder = AggregateBuilder()
        val corpusReader = createCorpusReader()
        corpusReader.uimaContext.logger.setLevel(Level.OFF)

        builder.add(AnalysisEngineFactory.createEngineDescription(
                ViewCreatorAnnotator::class.java,
                ViewCreatorAnnotator.PARAM_VIEW_NAME,
                "default"))

        builder.add(AnalysisEngineFactory.createEngineDescription(
                CorpusReader::class.java,
                CorpusReader.PARAM_PERCENTAGE_START, percentageStart.toFloat(),
                CorpusReader.PARAM_PERCENTAGE_END, percentageEnd.toFloat()
        ))

        builder.add(POSAnnotator
                .getClassifierDescription(JarClassifierBuilder.getModelJarFile(
                        File(modelOutput, featureSet).absolutePath).path, featureSet))

        builder.add(AnalysisEngineFactory.createEngineDescription(
                AnnotatedOutputWriter::class.java,
                AnnotatedOutputWriter.PARAM_OUTPUT_DIRECTORY_NAME, File(classificationOutput, featureSet),
                AnnotatedOutputWriter.PARAM_OUTPUT_FILE_NAME, File(classificationOutputFile)
        ))

        println("Testing model $featureSet")
        SimplePipeline.runPipeline(corpusReader, builder.createAggregateDescription())

        return verify(featureSet, percentageStart, percentageEnd)
    }

    private fun verify(featureSet: String, percentageStart: Double, percentageEnd: Double): Pair<String, VerificationOutput> {
        val verificator = AnnotationVerificator()
        val verificationResult = verificator.verify(corpusPath,
                percentageStart,
                percentageEnd,
                File(File(classificationOutput, featureSet), classificationOutputFile))
        println("Model $featureSet, Precision: ${verificationResult.precision}")
        return Pair(featureSet, verificationResult)
    }
}