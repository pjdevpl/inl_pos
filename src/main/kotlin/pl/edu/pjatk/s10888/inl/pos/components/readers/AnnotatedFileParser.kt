package pl.edu.pjatk.s10888.inl.pos.components.readers

import pl.edu.pjatk.s10888.inl.pos.model.AnnotatedCorpusEntry
import pl.edu.pjatk.s10888.inl.pos.model.AnnotatedSentence

class AnnotatedFileParser(private val lines: List<AnnotatedCorpusEntry>) {
    fun recognizeSentences(): List<AnnotatedSentence> {
        val terminalEnd = "&"
        val iterator = lines.listIterator()
        val sentence = mutableListOf<AnnotatedCorpusEntry>()
        val sentences = mutableListOf<AnnotatedSentence>()

        var previousEntry: AnnotatedCorpusEntry? = null
        var sentenceIndex = 0
        while (iterator.hasNext()) {
            if (previousEntry != null) {
                val entry = iterator.next()

                if (entry.word == terminalEnd && previousEntry.word.matches("\\W".toRegex())) {
                    val annotatedSentence = AnnotatedSentence(sentence.toList())
                    val beginningIndex = sentenceIndex - annotatedSentence.makeSentence().length
                    val endIndex = sentenceIndex

                    sentences.add(annotatedSentence.withDocumentPosition(beginningIndex, endIndex))
                    sentence.clear()
                } else {
                    val beginningIndex = sentenceIndex
                    val endIndex = beginningIndex + entry.word.length
                    sentence.add(entry.withSentencePosition(beginningIndex, endIndex))
                    sentenceIndex = endIndex + 1
                }
            } else {
                previousEntry = iterator.next()
            }
        }

        //add last sentence
        val annotatedSentence = AnnotatedSentence(sentence.toList())
        val beginningIndex = sentenceIndex - annotatedSentence.makeSentence().length
        val endIndex = sentenceIndex

        sentences.add(annotatedSentence.withDocumentPosition(beginningIndex, endIndex))

        return sentences.toList()
    }
}