package pl.edu.pjatk.s10888.inl.pos.components.verification

import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileParser
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileReader
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileSelector
import pl.edu.pjatk.s10888.inl.pos.model.AnnotatedCorpusEntry
import pl.edu.pjatk.s10888.inl.pos.model.VerificationOutput
import java.io.File
import java.lang.RuntimeException

class AnnotationVerificator {

    fun verify(inputFile: File, percentageStart: Double, percentageEnd: Double, classificationOutput: File): VerificationOutput {
        val testData = readTestData(inputFile, percentageStart, percentageEnd)
        val outputData = readOutputData(classificationOutput)

        if (testData.size != outputData.size) {
            throw RuntimeException("Test data and output data varies in size. Check the testing specification")
        }

        val assignedTags = mutableMapOf<String, Int>()
        val expectedAssignedTags = mutableMapOf<String, Int>()
        val correctlyAssigned = mutableMapOf<String, Int>()
        val incorrectlyAssigned = mutableMapOf<String, Int>()
        for (i in outputData.indices) {
            val testWord = testData[i]
            val annotatedWord = outputData[i]
            if (testWord.word != annotatedWord.word) {
                throw RuntimeException("Words in test data and output data are not aligned. Check the data specification")
            }

            increment(assignedTags, annotatedWord.tag.getFirstLevelTag())
            increment(expectedAssignedTags, testWord.tag.getFirstLevelTag())
            if (annotatedWord.tag.getFirstLevelTag() == testWord.tag.getFirstLevelTag()) {
                increment(correctlyAssigned, annotatedWord.tag.getFirstLevelTag())
            } else {
                increment(incorrectlyAssigned, annotatedWord.tag.getFirstLevelTag())
            }
        }

        val precision = correctlyAssigned.values.sum().toDouble() / assignedTags.values.sum().toDouble()
        return VerificationOutput(precision)
    }

    private fun readTestData(inputFile: File, percentageStart: Double, percentageEnd: Double): List<AnnotatedCorpusEntry> {
        val testDataReader = AnnotatedFileReader(inputFile)
        val parser = AnnotatedFileParser(testDataReader.readLinesFromCorpus())
        val selector = AnnotatedFileSelector(parser.recognizeSentences())
        val selectedSentences = selector.selectPercentage(percentageStart, percentageEnd)
        return selectedSentences.fold(mutableListOf()) { acc, annotatedSentence -> acc.addAll(annotatedSentence.annotatedWords); acc}
    }

    private fun readOutputData(classificationOutput: File): List<AnnotatedCorpusEntry> {
        val outputReader = AnnotatedFileReader(classificationOutput)
        val parser = AnnotatedFileParser(outputReader.readLinesFromTaggingOutput())
        return parser.recognizeSentences().flatMap { it.annotatedWords }
    }

    private fun increment(map: MutableMap<String, Int>, key: String) {
        map.merge(key, 1) { cur, inc -> cur + inc}
    }

}