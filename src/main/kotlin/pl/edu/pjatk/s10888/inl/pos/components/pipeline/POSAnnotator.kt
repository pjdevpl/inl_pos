package pl.edu.pjatk.s10888.inl.pos.components.pipeline

import org.apache.uima.UimaContext
import org.apache.uima.analysis_engine.AnalysisEngineDescription
import org.apache.uima.fit.descriptor.ConfigurationParameter
import org.apache.uima.fit.factory.AnalysisEngineFactory
import org.apache.uima.fit.util.JCasUtil
import org.apache.uima.jcas.JCas
import org.cleartk.ml.CleartkSequenceAnnotator
import org.cleartk.ml.Instance
import org.cleartk.ml.crfsuite.CrfSuiteStringOutcomeDataWriter
import org.cleartk.ml.feature.extractor.CleartkExtractor
import org.cleartk.ml.feature.function.FeatureFunctionExtractor
import org.cleartk.ml.jar.DefaultDataWriterFactory
import org.cleartk.ml.jar.DirectoryDataWriterFactory
import org.cleartk.ml.jar.GenericJarClassifierFactory
import org.cleartk.token.type.Sentence
import org.cleartk.token.type.Token
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.variants.FeatureSet
import pl.edu.pjatk.s10888.inl.pos.utils.ParamsInitializer


class POSAnnotator : CleartkSequenceAnnotator<String>() {

    companion object {
        fun getWriterDescription(outputDirectory: String, featureId: String): AnalysisEngineDescription {
            return AnalysisEngineFactory.createEngineDescription(
                    POSAnnotator::class.java,
                    PARAM_VARIANT,
                    featureId,
                    DirectoryDataWriterFactory.PARAM_OUTPUT_DIRECTORY,
                    outputDirectory,
                    DefaultDataWriterFactory.PARAM_DATA_WRITER_CLASS_NAME,
                    CrfSuiteStringOutcomeDataWriter::class.java.name)
        }

        fun getClassifierDescription(modelFileName: String, featureId: String): AnalysisEngineDescription {
            return AnalysisEngineFactory.createEngineDescription(
                    POSAnnotator::class.java,
                    PARAM_VARIANT,
                    featureId,
                    GenericJarClassifierFactory.PARAM_CLASSIFIER_JAR_PATH,
                    modelFileName)
        }

        const val PARAM_VARIANT = "paramVariant"
    }

    @ConfigurationParameter(name = PARAM_VARIANT, mandatory = true, description = "defines, which featureset variant to choose from")
    lateinit var variant: String

    private lateinit var tokenFeatureExtractor: FeatureFunctionExtractor<Token>
    private lateinit var contextFeatureExtractor: CleartkExtractor<Token, Token>

    override fun initialize(context: UimaContext) {
        super.initialize(context)
        ParamsInitializer.initializeParameters(this, context)
        val featureSet = FeatureSet.getFeatureSet(variant)
        this.tokenFeatureExtractor = featureSet.tokenFeatureExtractor
        this.contextFeatureExtractor = featureSet.contextFeatureExtractor
    }

    override fun process(aJCas: JCas?) {
        val sentences = JCasUtil.select(aJCas, Sentence::class.java)
        val sentencesCount = sentences.size
        sentences.forEachIndexed { index, sentence ->
            val tokens = JCasUtil.selectCovered(aJCas, Token::class.java, sentence)

            val tokenInstances = tokens.map { token ->
                val tokenFeatures = this.tokenFeatureExtractor.extract(aJCas, token)
                val tokenContextFeatures = this.contextFeatureExtractor.extractWithin(aJCas, token, sentence)
                Instance(token.pos, tokenFeatures + tokenContextFeatures)
            }

            if (this.isTraining) {
                print("\rWriting features: ${index+1}/$sentencesCount")
                this.dataWriter.write(tokenInstances)
            } else {
                print("\rClassifying: ${index+1}/$sentencesCount")

                val predictedOutcomes = this.classifier.classify(tokenInstances.map { it.features })
                val tokensIter = tokens.iterator()
                for (outcome in predictedOutcomes) {
                    tokensIter.next().pos = outcome
                }
            }
        }
        println()
    }
}