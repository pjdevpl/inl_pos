package pl.edu.pjatk.s10888.inl.pos.model

class AnnotatedDocument(val annotatedSentences: List<AnnotatedSentence>) {

    fun makeDocument(): String {
        val stringBuilder = StringBuilder()

        annotatedSentences.forEach { sentence ->
            sentence.annotatedWords.forEach { word ->
                stringBuilder.append(word.word)
                stringBuilder.append(" ")
            }
        }

        return stringBuilder.toString()
    }

}