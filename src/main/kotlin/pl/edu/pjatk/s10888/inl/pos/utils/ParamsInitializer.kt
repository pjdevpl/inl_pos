package pl.edu.pjatk.s10888.inl.pos.utils

import org.apache.uima.UimaContext
import org.apache.uima.fit.descriptor.ConfigurationParameter
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.javaField

@Suppress("UNCHECKED_CAST")
object ParamsInitializer {

    fun <T : Any> initializeParameters(component: T, context: UimaContext) {
        val c = component::class.memberProperties
                .filter { it.javaField!!.isAnnotationPresent(ConfigurationParameter::class.java) }
                .map {
                    Pair(it, it.javaField!!.getAnnotation(ConfigurationParameter::class.java))
                }

        c.forEach { (field, propertyAnnotation) ->
            val mutableField = (field as KMutableProperty1<T, Any>)
            mutableField.set(component, context.getConfigParameterValue(propertyAnnotation.name))
        }
    }
}