package pl.edu.pjatk.s10888.inl.pos.components.pipeline.variants

import org.cleartk.ml.feature.extractor.CleartkExtractor
import org.cleartk.ml.feature.extractor.CoveredTextExtractor
import org.cleartk.ml.feature.function.*
import org.cleartk.token.type.Token

class FeatureSetA: FeatureSet {

    override val tokenFeatureExtractor = FeatureFunctionExtractor(
            CoveredTextExtractor<Token>(),
            LowerCaseFeatureFunction(),
            CapitalTypeFeatureFunction(),
            NumericTypeFeatureFunction(),
            CharacterNgramFeatureFunction(CharacterNgramFeatureFunction.Orientation.RIGHT_TO_LEFT, 0, 2),
            CharacterNgramFeatureFunction(CharacterNgramFeatureFunction.Orientation.RIGHT_TO_LEFT, 0, 3))

    override val contextFeatureExtractor = CleartkExtractor<Token, Token>(
            Token::class.java,
            CoveredTextExtractor(),
            CleartkExtractor.Preceding(1),
            CleartkExtractor.Following(1))
}