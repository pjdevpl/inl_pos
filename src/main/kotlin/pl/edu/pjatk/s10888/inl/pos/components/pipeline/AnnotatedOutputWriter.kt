package pl.edu.pjatk.s10888.inl.pos.components.pipeline

import org.apache.uima.UimaContext
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase
import org.apache.uima.fit.descriptor.ConfigurationParameter
import org.apache.uima.fit.util.JCasUtil
import org.apache.uima.jcas.JCas
import org.cleartk.token.type.Sentence
import org.cleartk.token.type.Token
import pl.edu.pjatk.s10888.inl.pos.utils.ParamsInitializer
import java.io.File
import java.io.PrintWriter


class AnnotatedOutputWriter : JCasAnnotator_ImplBase() {

    companion object {
        const val PARAM_OUTPUT_DIRECTORY_NAME = "outputDirectoryName"
        const val PARAM_OUTPUT_FILE_NAME = "outputFileName"
    }

    @ConfigurationParameter(name = PARAM_OUTPUT_DIRECTORY_NAME, mandatory = true, description = "provides the directory where the token/pos text files will be written")
    var outputDirectoryPath: String? = null

    @ConfigurationParameter(name = PARAM_OUTPUT_FILE_NAME, mandatory = true, description = "provides the name of the file being written")
    var outputFileName: String? = null

    override fun initialize(context: UimaContext) {
        super.initialize(context)
        ParamsInitializer.initializeParameters(this, context)
    }

    override fun process(jCas: JCas) {
        val outputDirectory = File(outputDirectoryPath)

        if (!outputDirectory.exists()) {
            outputDirectory.mkdirs()
        }

        val outputWriter = PrintWriter(File(outputDirectory, outputFileName))

        JCasUtil.select(jCas, Sentence::class.java).forEach { sentence ->
            outputWriter.println("&\t&")
            JCasUtil.selectCovered(jCas, Token::class.java, sentence).forEach { token ->
                outputWriter.print(token.coveredText)
                outputWriter.print('\t')
                outputWriter.print(token.pos)
                outputWriter.println()
            }
        }
        outputWriter.println("&\t&")
        outputWriter.close()
    }
}